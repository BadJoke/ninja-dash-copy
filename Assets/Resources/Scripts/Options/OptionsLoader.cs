﻿using UnityEngine;
using UnityEngine.UI;

public class OptionsLoader : MonoBehaviour
{
    [SerializeField] private Text _levelNumber;

    [Space]
    [SerializeField] private int _level;
    [SerializeField] private float _activeTimeDuration;

    [Header("Player")]
    [SerializeField] private float _playerPushPower;
    [SerializeField] private float _playerPushRadius;
    [SerializeField] private float _killRadius;

    [Header("Enemies: Simple")]
    [SerializeField] private float _enemyDetectionDistance;
    [SerializeField] private float _enemyMovementDistance;
    [Header("Strong")]
    [SerializeField] private float _strongEnemyDetectionDistance;
    [SerializeField] private float _strongEnemyMovementDistance;
    [Header("Pushing")]
    [SerializeField] private float _enemyPushPower;
    [SerializeField] private float _enemyPushRadius;
    [SerializeField] private float _enemyHitPushingDistance;

    private const string _levelString = "LEVEL ";

    private void Awake()
    {
        if (_level <= 0)
        {
            if (PlayerPrefs.HasKey("Level"))
            {
                Options.CurrentLevel = PlayerPrefs.GetInt("Level");
            }
            else
            {
                Options.CurrentLevel = 1;
            }
        }
        else
        {
            Options.CurrentLevel = _level;
        }

        Random.InitState(Options.CurrentLevel);

        if (Options.CurrentLevel > 20)
        {
            Options.LoadingLevel = Random.Range(5, 21);
        }
        else
        {
            Options.LoadingLevel = Options.CurrentLevel;
        }

        _levelNumber.text = _levelString + Options.CurrentLevel;

        Options.ActiveTimeDuration = _activeTimeDuration;
        Options.PlayerPushPower = _playerPushPower;
        Options.PlayerPushRadius = _playerPushRadius;
        Options.MoveOffset = _killRadius;

        Options.EnemyDetectionDistance = _enemyDetectionDistance;
        Options.EnemyMovementDistance = _enemyMovementDistance;
        Options.StrongEnemyDetectionDistance = _strongEnemyDetectionDistance;
        Options.StrongEnemyMovementDistance = _strongEnemyMovementDistance;
        Options.EnemyPushPower = _enemyPushPower;
        Options.EnemyPushRadius = _enemyPushRadius;
        Options.EnemyHitPushingDistance = _enemyHitPushingDistance;
    }
}
