﻿public static class Options
{
    public static int CurrentLevel;
    public static int LoadingLevel;
    public static int RestartCount = 0;

    public static float ActiveTimeDuration = 1f;
    public static float MoveOffset = 3.2f;
    public static string SavePositionsPath = "Assets/Resources/Scripts/EnemyPosition/ReadyLevels/LevelPositions{0}.asset";
    public static string LoadPositionsPath = "Scripts/EnemyPosition/ReadyLevels/LevelPositions{0}";

    public static float PlayerPushPower;
    public static float PlayerPushRadius;

    public static float EnemyDetectionDistance;
    public static float EnemyMovementDistance;
    public static float StrongEnemyDetectionDistance;
    public static float StrongEnemyMovementDistance;
    public static float EnemyPushPower;
    public static float EnemyPushRadius;
    public static float EnemyHitPushingDistance;
}