﻿using UnityEngine;

public abstract class Stickman : MonoBehaviour
{
    [SerializeField] protected Animator animator;

    protected Rigidbody[] limbs;

    protected void Start()
    {
        limbs = GetComponentsInChildren<Rigidbody>();
    }

    public abstract void TakeHit(Vector3 pushPoint, bool isLeft);

    protected abstract void Die(Vector3 pushPoint);

    protected void ActivateRagdoll()
    {
        animator.enabled = false;

        foreach (var limb in limbs)
        {
            limb.useGravity = true;
            limb.isKinematic = false;
        }
    }
}
