﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : Stickman
{
    [SerializeField] protected float detectionDistance;
    [SerializeField] protected float movementDistance;
    [SerializeField] protected float movementOffset;
    [SerializeField] protected Rigidbody head;
    [SerializeField] protected ParticleSystem sparksLeft;
    [SerializeField] protected ParticleSystem sparksRight;
    [SerializeField] protected EnemyDigit digit;
    [SerializeField] private Transform _pushPoint;

    public UnityAction<Enemy> Died;
    
    protected Player player;

    private Collider _collider;
    private Tween _moving;
    private bool _needStopAnimation = true;

    protected new void Start()
    {
        base.Start();

        detectionDistance = Options.EnemyDetectionDistance;
        movementDistance = Options.EnemyMovementDistance;

        _collider = GetComponent<Collider>();
        player = FindObjectOfType<Player>();
        player.DashEnded += OnDashEnded;
        player.Died += StopDash;

        transform.LookAt(player.transform);
    }

    private void OnDestroy()
    {
        if (player)
        {
            player.DashEnded -= OnDashEnded;
            player.Died -= StopDash;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, detectionDistance);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, movementDistance);
    }

    public void SetAsTarget()
    {
        digit.Show();
    }

    public int GetIntDigit()
    {
        return Convert.ToInt32(digit.Get());
    }

    public string GetDigit()
    {
        return digit.Get();
    }

    public void SetDigit(string value)
    {
        digit.Set(value);
    }

    public void StopDash()
    {
        if (_needStopAnimation)
        {
            animator.speed = 0;
        }

        _moving?.Kill(false);
    }

    public override void TakeHit(Vector3 pushPoint, bool isLeft)
    {
        Die(pushPoint);

        if (isLeft)
        {
            sparksLeft.transform.parent.LookAt(pushPoint);
            sparksLeft.Play();
        }
        else
        {
            sparksRight.transform.parent.LookAt(pushPoint);
            sparksRight.Play();
        }
    }

    protected new void ActivateRagdoll()
    {
        _collider.enabled = false;

        base.ActivateRagdoll();
    }

    protected override void Die(Vector3 pushPoint)
    {
        ActivateRagdoll();
        HideDigit();
        Died?.Invoke(this);

        foreach (var limb in limbs)
        {
            limb.AddExplosionForce(Options.PlayerPushPower, pushPoint, Options.PlayerPushRadius);
        }

        head.transform.parent = null;
        head.GetComponent<Collider>().enabled = true;
        head.AddExplosionForce(Options.PlayerPushPower / 3f, pushPoint, Options.PlayerPushRadius);

        player.DashEnded -= OnDashEnded;
    }

    protected void Kill()
    {
        player.TakeHit(_pushPoint.position);
    }

    protected void OnDashEnded()
    {
        Vector3 targetPostion = player.transform.position;
        float distance = Vector3.Distance(transform.position, targetPostion);

        if (distance <= detectionDistance)
        {
            transform.LookAt(player.transform);
            animator.speed = 1;
            distance -= Options.MoveOffset;

            Vector3 direction = targetPostion - transform.position;
            Vector3 path;

            if (distance > movementDistance)
            {
                animator.SetTrigger("Run");
                if (distance - movementDistance < movementOffset)
                {
                    path = direction.normalized * (movementDistance - movementOffset);
                }
                else
                {
                    path = direction.normalized * movementDistance;
                }
            }
            else
            {
                animator.SetTrigger("Attack");
                path = direction.normalized * distance;
                _needStopAnimation = false;
            }

            Vector3 killPosition = transform.position + path;
            _moving = transform.DOMove(killPosition, Options.ActiveTimeDuration * 0.699f).SetEase(Ease.Linear);

            if (_needStopAnimation)
            {
                _moving.onComplete = () =>
                {
                    animator.speed = 0;
                };
            }
        }
    }

    public void HideDigit()
    {
        digit.Hide();
    }
}
