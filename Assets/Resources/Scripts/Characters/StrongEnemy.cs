﻿using DG.Tweening;
using UnityEngine;

public class StrongEnemy : Enemy
{
    [SerializeField] private float _hittedPushDistance;
    [SerializeField] private ParticleSystem _hitEffect;

    private short _health = 2;
    private bool _isNotStanned = true;

    private new void Start()
    {
        base.Start();

        detectionDistance = Options.StrongEnemyDetectionDistance;
        movementDistance = Options.StrongEnemyMovementDistance;

        player.DashEnded -= base.OnDashEnded;
        player.DashEnded += OnDashEnded;
    }

    public void PlayHit()
    {
        _hitEffect.Play();
    }

    public override void TakeHit(Vector3 pushPoint, bool isLeft)
    {
        if (--_health <= 0)
        {
            if (isLeft)
            {
                sparksLeft.transform.parent.LookAt(pushPoint);
                sparksLeft.Play();
            }
            else
            {
                sparksRight.transform.parent.LookAt(pushPoint);
                sparksRight.Play();
            }

            Die(pushPoint);
            player.DashEnded -= OnDashEnded;
        }
        else
        {
            animator.speed = 1f;
            animator.SetTrigger("Protect");
            _isNotStanned = false;

            Vector3 pushDirection = (transform.position - pushPoint).normalized;
            pushDirection.y = 0f;
            Vector3 pushEndPosition = transform.position + pushDirection * Options.EnemyHitPushingDistance;
            transform.DOMove(pushEndPosition, 0.3f);
        }
    }

    private new void OnDashEnded()
    {
        if (_isNotStanned)
        {
            base.OnDashEnded();
        }
        else
        {
            transform.LookAt(player.transform);
            _isNotStanned = true;
        }
    }
}
