﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;

public class Player : Stickman
{
    [SerializeField] private Transform _pushPoint;
    [SerializeField] private Transform _player;
    [SerializeField] private Transform _hat;
    [SerializeField] private LayerMask _enemyLayer;
    [SerializeField] private CameraShaker _shaker;
    [SerializeField] private Rigidbody _head;
    [SerializeField] private Transform _katana;
    [SerializeField] private KatanaPosition _katanaPositionFirst;
    [SerializeField] private KatanaPosition _katanaPositionSecond;
    [SerializeField] private GhostTrailEffect _ghostTrail;
    [SerializeField] private EnemyCounter _counter;

    public UnityAction DashStarted;
    public UnityAction DashEnded;
    public UnityAction Died;

    private bool _isFirstAttack = true;
    private bool _canKill = true;
    private Camera _camera;

    private new void Start()
    {
        _camera = Camera.main;

        base.Start();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _canKill)
        {
            if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
            {
                if (hit.transform.TryGetComponent(out Enemy enemy))
                {
                    Kill(enemy);
                }
            }
        }
    }

    public void AllowKilling()
    {
        _canKill = true;
    }

    public void StoplIdleSwitching()
    {
        animator.SetBool("IsMenu", false);
        animator.SetBool("IsGame", true);
    }

    public void TakeHit(Vector3 pushPoint)
    {
        TakeHit(pushPoint, true);
    }

    public override void TakeHit(Vector3 pushPoint, bool isLeft)
    {
        Die(pushPoint);
    }

    protected override void Die(Vector3 pushPoint)
    {
        ActivateRagdoll();
        Died?.Invoke();

        foreach (var limb in limbs)
        {
            limb.AddExplosionForce(Options.EnemyPushPower, pushPoint, Options.EnemyPushRadius);
        }

        _head.transform.parent = null;
        _head.GetComponent<Collider>().enabled = true;
        _head.AddExplosionForce(Options.EnemyPushPower / 3f, pushPoint, Options.EnemyPushRadius);
        _hat.DOScale(new Vector3(1.7f, 1.7f, 1.7f), 0.2f);

        enabled = false;
    }

    protected void PrepareToAttack()
    {
        Attack(_isFirstAttack ? _katanaPositionFirst : _katanaPositionSecond);
    }

    private void Attack(KatanaPosition katana)
    {
        animator.SetTrigger(katana.Clip);
        animator.speed = 0.1f;
        _katana.localEulerAngles = katana.Rotation;
    }

    private void Kill(Enemy enemy)
    {
        Vector3 targetPostion = enemy.transform.position;
        Vector3 path = targetPostion - _player.position;
        path -= path.normalized * Options.MoveOffset;
        Vector3 preKillPosition = _player.position + path * 0.8f;
        Vector3 killPosition = _player.position + path;
        transform.LookAt(enemy.transform);

        _canKill = false;
        _ghostTrail.StartSpawn(_isFirstAttack);

        DashStarted?.Invoke();
        PrepareToAttack();

        float distance = Vector3.Distance(_player.position, targetPostion);
        float duration = Options.ActiveTimeDuration * 0.25f;

        var startDash = _player
            .DOMove(preKillPosition, duration * 0.8f)
            .SetEase(Ease.Linear)
            .Pause();
        startDash.onComplete = () =>
        {
            animator.speed = 1;

            if (_isFirstAttack)
            {
                _katanaPositionFirst.PlayEffect();
            }
            else
            {
                _katanaPositionSecond.PlayEffect();
            }
        };

        var endDash = _player
            .DOMove(killPosition, duration * 0.2f)
            .SetEase(Ease.Linear)
            .Pause();
        endDash.onComplete = () =>
        { 
            enemy.TakeHit(_pushPoint.position, _isFirstAttack);
            DashEnded?.Invoke();

            if (_counter.Сount > 1)
            {
                _shaker.Shake(0.15f, 0.2f);
            }

            _ghostTrail.StopSpawn();
            _isFirstAttack = !_isFirstAttack;
        };

        var dash = DOTween.Sequence();
        dash.Append(startDash);
        dash.Append(endDash);
    }

    [Serializable]
    private struct KatanaPosition
    {
        [SerializeField] private string _animClip;
        [SerializeField] private Vector3 _position;
        [SerializeField] private Vector3 _rotation;
        [SerializeField] private ParticleSystem _effect;

        public string Clip => _animClip;
        public Vector3 Position => _position;
        public Vector3 Rotation => _rotation;

        public void PlayEffect()
        {
            _effect.Play();
        }
    }
}
