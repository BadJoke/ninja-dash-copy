﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyPositionSaver : MonoBehaviour
{
    [SerializeField] private int _level = 1;
    [SerializeField] private EnemySpawner _spawner;

    private void OnValidate()
    {
        if (_level < 1)
        {
            _level = 1;
        }
    }

    public void CreatePositions()
    {
        var enemies = new List<Enemy>(FindObjectsOfType<Enemy>());
        var strongEnemies = new List<StrongEnemy>(FindObjectsOfType<StrongEnemy>());
        List<EnemyPosition> enemyPositions = new List<EnemyPosition>();
        List<EnemyPosition> strongEnemyPositions = new List<EnemyPosition>();

        foreach (var enemy in strongEnemies)
        {
            enemies.Remove(enemy);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            var position = enemies[i].transform.position;
            position.y = 0;

            var enemyPosition = new EnemyPosition(enemies[i].GetDigit(), position);
            enemyPositions.Add(enemyPosition);
        }

        for (int i = 0; i < strongEnemies.Count; i++)
        {
            var position = strongEnemies[i].transform.position;
            position.y = 0;

            var enemyPosition = new EnemyPosition(strongEnemies[i].GetDigit(), position);
            strongEnemyPositions.Add(enemyPosition);
        };

        EnemyPositions asset = ScriptableObject.CreateInstance<EnemyPositions>();

        asset.EnemyPosition = enemyPositions.ToArray();
        asset.StrongEnemyPosition = strongEnemyPositions.ToArray();

        AssetDatabase.CreateAsset(asset, string.Format(Options.SavePositionsPath, _level++));
        AssetDatabase.SaveAssets();
    }

    public void LoadPositions()
    {
        _spawner.Spawn(_level);
    }

    public void ClearPositions()
    {
        foreach (var enemy in FindObjectsOfType<Enemy>())
        {
            DestroyImmediate(enemy.gameObject);
        }
    }
}
#endif