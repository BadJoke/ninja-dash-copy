﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EnemyPositionSaver))]
public class EnemyPositionSaverEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var saver = (EnemyPositionSaver)target;

        if (GUILayout.Button("Save"))
        {
            saver.CreatePositions();
        }

        if (GUILayout.Button("Load"))
        {
            saver.LoadPositions();
        }

        if (GUILayout.Button("Clear"))
        {
            saver.ClearPositions();
        }
    }
}
#endif
