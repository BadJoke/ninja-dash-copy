﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyPositions")]
public class EnemyPositions : ScriptableObject
{
    public EnemyPosition[] EnemyPosition;
    public EnemyPosition[] StrongEnemyPosition;
}

[Serializable]
public struct EnemyPosition
{
    public string Number;
    public Vector3 Position;

    public EnemyPosition(string number, Vector3 position)
    {
        Number = number;
        Position = position;
    }
}