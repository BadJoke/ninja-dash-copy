﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Enemy _enemyPrefab;
    [SerializeField] private StrongEnemy _strongEnemyPrefab;

    public UnityAction<List<Enemy>> Spawned;
    public UnityAction<Enemy[]> Sorted;

    private List<Enemy> _allEnemies = new List<Enemy>();

    private void Start()
    {
        Spawn(Options.LoadingLevel);
        Spawned?.Invoke(_allEnemies);
    }

    public void Spawn(int level)
    {
        string path = string.Format(Options.LoadPositionsPath, level);

        var levelPositions = Resources.Load<EnemyPositions>(path);
        var enemyPositions = levelPositions.EnemyPosition;
        var strongEnemyPositions = levelPositions.StrongEnemyPosition;

        if (enemyPositions != null)
        {
            CreateEnemy(enemyPositions, _enemyPrefab);
        }

        if (strongEnemyPositions != null)
        {
            CreateEnemy(strongEnemyPositions, _strongEnemyPrefab);
        }

        Sort();
    }

    private void Sort()
    {
        var sortedEnemies = (from Enemy enemy in _allEnemies
                             orderby enemy.GetIntDigit()
                             select enemy).ToArray();

        Sorted?.Invoke(sortedEnemies);
    }

    private void CreateEnemy(EnemyPosition[] enemyPositions, Enemy prefab)
    {
        foreach (var position in enemyPositions)
        {
            var enemy = Instantiate(prefab, position.Position, Quaternion.identity);
            enemy.SetDigit(position.Number);
            _allEnemies.Add(enemy);
        }
    }
}
