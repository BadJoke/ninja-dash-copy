﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EnemyCounter : MonoBehaviour
{
    [SerializeField] private Text _amount;
    [SerializeField] private EnemySpawner _spawner;
    [SerializeField] private KillHelper _helper;

    private int _count;

    public int Сount => _count;

    public UnityAction AllEnemyDied;

    private void OnEnable()
    {
        _spawner.Spawned += SetEnemies;
    }

    private void OnDisable()
    {
        _spawner.Spawned -= SetEnemies;
    }

    public void SetEnemies(List<Enemy> enemies)
    {
        _count = enemies.Count;
        _amount.text = _count.ToString();

        foreach (var enemy in enemies)
        {
            enemy.Died += OnEnemyDied;
        }
    }

    private void OnEnemyDied(Enemy enemy)
    {
        enemy.Died -= OnEnemyDied;

        _count--;
        _amount.text = _count.ToString();

        if (Сount == 0)
        {
            AllEnemyDied?.Invoke();
        }
    }
}
