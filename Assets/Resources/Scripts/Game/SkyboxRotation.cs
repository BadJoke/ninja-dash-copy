﻿using UnityEngine;

public class SkyboxRotation : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed;

    private float _angle = 0;

    private void Update()
    {
        _angle += _rotationSpeed * Time.unscaledDeltaTime;

        if (_angle > 360)
        {
            _angle -= 360;
        }

        RenderSettings.skybox.SetFloat("_Rotation", _angle);
    }
}
