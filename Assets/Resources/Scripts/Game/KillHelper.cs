﻿using UnityEngine;

public class KillHelper : MonoBehaviour
{
    [SerializeField] private EnemySpawner _spawner;

    private Enemy[] _enemies;
    private float _restartRatio;
    private int _pointer = 0;
    private bool _isInterrupted = false;

    private void Start()
    {
        var count = Options.RestartCount - 2;

        if (count < 0)
        {
            return;
        }

        _restartRatio = (count + 1) * 0.1f;
    }

    private void OnEnable()
    {
        _spawner.Sorted += OnSorted;
    }

    private void OnSorted(Enemy[] enemies)
    {
        _spawner.Sorted -= OnSorted;

        _enemies = enemies;

        if (Options.RestartCount > 1)
        {
            _enemies[0].SetAsTarget();
        }

        foreach (var enemy in _enemies)
        {
            enemy.Died += OnEnemyDied;
        }
    }

    private void OnEnemyDied(Enemy enemy)
    {
        _pointer++;

        if (_pointer == enemy.GetIntDigit())
        {
            if (_isInterrupted == false)
            {
                if (_pointer <= _enemies.Length * _restartRatio)
                {
                    _enemies[_pointer].SetAsTarget();
                }
            }
        }
        else
        {
            _enemies[_pointer - 1].HideDigit();
            _isInterrupted = true;
        }
    }
}
