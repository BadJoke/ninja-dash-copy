﻿using System.Collections;
using UnityEngine;

public class TimeKeeper : MonoBehaviour
{
    private Player _player;
    private EnemyCounter _counter;

    private void OnEnable()
    {
        _player = FindObjectOfType<Player>();
        _player.DashStarted += OnDashStarted;
        _player.Died += OnEnded;
        _counter = FindObjectOfType<EnemyCounter>();
        _counter.AllEnemyDied += OnEnded;
    }

    private void OnDisable()
    {
        _player.DashStarted -= OnDashStarted;
        _counter.AllEnemyDied -= OnEnded;
    }

    private void OnDashStarted()
    {
        StopCoroutine("ToggleTime");
        StartCoroutine("ToggleTime");
    }

    private IEnumerator ToggleTime()
    {
        Time.timeScale = 1f;
        yield return new WaitForSeconds(Options.ActiveTimeDuration);
        Time.timeScale = 0f;
        _player.AllowKilling();
    }

    private void OnEnded()
    {
        StopCoroutine("ToggleTime");
        Time.timeScale = 0.35f;
    }
}
