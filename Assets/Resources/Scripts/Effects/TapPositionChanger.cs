﻿using UnityEngine;

public class TapPositionChanger : MonoBehaviour
{
    private static RectTransform _transform;

    public static Vector2 RandomPosition()
    {
        float width = _transform.rect.width;
        float height = _transform.rect.height;
        var randomedPosition = new Vector3(Random.Range(-width / 2, width / 2), Random.Range(-height / 2, height / 2));
        return randomedPosition + _transform.localPosition;
    }

    private void Start()
    {
        _transform = (RectTransform)transform;
    }
}
