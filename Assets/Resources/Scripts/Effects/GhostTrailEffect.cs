﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostTrailEffect : MonoBehaviour
{
    [SerializeField] private float _interval;
    [SerializeField] private Ghost _ghost;
    [SerializeField] private Transform _player;

    private GhostPool _pool;
    private Coroutine _spawning;

    private void Start()
    {
        _pool = new GhostPool(_ghost, transform);
    }

    public void StartSpawn(bool isFirstAttack)
    {
        _spawning = StartCoroutine(Spawning(isFirstAttack));
    }

    public void StopSpawn()
    {
        StopCoroutine(_spawning);
    }

    private IEnumerator Spawning(bool isFirstAttack)
    {
        while (true)
        {
            var ghost = _pool.Take();
            ghost.Activate(isFirstAttack);
            ghost.transform.position = _player.position;
            ghost.transform.rotation = _player.rotation;

            yield return new WaitForSeconds(_interval);
        }
    }
}

public struct GhostPool
{
    private int _startValue;
    private Ghost _ghost;
    private Transform _parent;
    private Queue<Ghost> _ghosts;

    public GhostPool(Ghost ghost, Transform parent)
    {
        _startValue = 5;
        _ghost = ghost;
        _parent = parent;
        _ghosts = new Queue<Ghost>();

        for (int i = 0; i < _startValue; i++)
        {
            Create();
        }
    }

    public Ghost Take()
    {
        if (_ghosts.Count == 0)
        {
            Create();
        }

        var ghost = _ghosts.Dequeue();
        ghost.transform.parent = null;
        ghost.gameObject.SetActive(true);
        return ghost;
    }

    private void Create()
    {
        var newGhost = Object.Instantiate(_ghost, _parent);
        newGhost.Init(this);
        newGhost.gameObject.SetActive(false);
        _ghosts.Enqueue(newGhost);
    }

    public void Put(Ghost ghost)
    {
        ghost.gameObject.SetActive(false);
        _ghosts.Enqueue(ghost);
    }
}