﻿using DG.Tweening;
using UnityEngine;

public class PointerMovement : MonoBehaviour
{
    private void Start()
    {
        float start = transform.localPosition.x;
        float finish = -start;

        var sequence = DOTween.Sequence();
        sequence.Append(transform.DOLocalMoveX(finish, 1.4f));
        sequence.Append(transform.DOLocalMoveX(start, 1.4f));
        sequence.SetLoops(-1);
    }
}
