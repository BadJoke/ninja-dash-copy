﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TapEffect : MonoBehaviour
{
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private Image _image;
    [SerializeField] private bool _canRandomChangePosition;

    private Sequence _sequence;

    private void Start()
    {
        SetSequence();

        if (_canRandomChangePosition)
        {
            transform.localPosition = TapPositionChanger.RandomPosition();

            _sequence.onStepComplete += RandomChagingPosition;
        }
    }

    private void SetSequence()
    {
        _sequence = DOTween.Sequence().SetEase(Ease.Linear).SetLoops(-1).SetUpdate(true);
        _sequence.Append(_rectTransform.DOSizeDelta(Vector2.one, 0.5f));
        _sequence.Join(_rectTransform.DOScale(Vector3.one * 0.75f, 0.5f));
        _sequence.Append(_rectTransform.DOSizeDelta(Vector2.one * 125f, 1f));
        _sequence.Join(_rectTransform.DOScale(Vector3.one, 1f));
        _sequence.Join(_image.DOFade(0f, 1f));
    }

    private void RandomChagingPosition()
    {
        transform.localPosition = TapPositionChanger.RandomPosition();
    }
}
