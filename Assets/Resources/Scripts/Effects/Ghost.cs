﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    [SerializeField] private float _fadingTime = 0.7f;
    [SerializeField] private Animator _animator;
    
    private GhostPool _pool;

    public void Init(GhostPool pool)
    {
        _pool = pool;
    }

    public void Activate(bool isFirstAttack)
    {
        _animator.SetBool("IsFirst", isFirstAttack);
        gameObject.SetActive(true);
        Invoke("BackToPool", _fadingTime);
    }

    private void BackToPool()
    {
        _pool.Put(this);
    }
}
