﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonClickEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Sprite _pressed;

    private Sprite _main;
    private Image _image;

    private void Start()
    {
        _image = GetComponent<Image>();
        _main = _image.sprite;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _image.sprite = _pressed;
        transform.Translate(Vector3.down * 1.6f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _image.sprite = _main;
        transform.Translate(Vector3.up * 1.6f);
    }
}
