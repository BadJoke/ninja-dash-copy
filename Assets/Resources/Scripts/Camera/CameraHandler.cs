﻿using System;
using System.Collections;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    [SerializeField] private Transform _hat;
    [SerializeField] private Transform _light;
    [SerializeField] private MeshRenderer _hatRenderer;

    [Space]
    [SerializeField] private Transform _axis;

    [Space]
    [SerializeField] private Transform _startLocation;
    [SerializeField] private Transform _finalLocation;

    [Space]
    [SerializeField] private Transform _startAxis;
    [SerializeField] private Transform _finalAxis;

    private Vector3 _hatScale = new Vector3(1.35f, 1.35f, 1.35f);
    private Vector3 _hatPosition = new Vector3(0f, 0.58f, 0f);
    private Vector3 _lightRotation = new Vector3(80f, 157.76f, 0);
    private float _ratio = 1.014f;

    public void Start()
    {
        transform.localPosition = _startLocation.localPosition;
        transform.localRotation = _startLocation.localRotation;
        _axis.localRotation = _startAxis.localRotation;
    }

    public void RaiseCamera(float delay, float cameraSpeed, float axisSpeed, Action endingAction)
    {
        StartCoroutine(SetLocation(delay, cameraSpeed, axisSpeed, endingAction));
    }

    private void SetEndValues()
    {
        transform.localPosition = _finalLocation.localPosition;
        transform.localRotation = _finalLocation.localRotation;
        _axis.localRotation = _finalAxis.localRotation;
    }

    private IEnumerator SetLocation(float delay, float cameraSpeed, float axisSpeed, Action endingAction)
    {
        yield return new WaitForSeconds(delay);

        _hatRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;

        while (Vector3.Distance(transform.localPosition, _finalLocation.localPosition) > 0.01f 
            || Quaternion.Angle(_axis.localRotation, _finalAxis.localRotation) > 0.1f)
        {
            float deltaTime = Time.deltaTime;

            transform.localPosition = Vector3.Slerp(transform.localPosition, _finalLocation.localPosition, cameraSpeed * deltaTime);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, _finalLocation.localRotation, cameraSpeed * deltaTime);
            _axis.localRotation = Quaternion.Slerp(_axis.localRotation, _finalAxis.localRotation, axisSpeed * deltaTime);
            _hat.localScale = Vector3.Slerp(_hat.localScale, _hatScale, cameraSpeed * deltaTime);
            _hat.localPosition = Vector3.Slerp(_hat.localPosition, _hatPosition, cameraSpeed * deltaTime);
            _light.rotation = Quaternion.Lerp(_light.rotation, Quaternion.Euler(_lightRotation), cameraSpeed * deltaTime);

            cameraSpeed *= _ratio;
            axisSpeed *= _ratio;

            yield return null;
        }

        SetEndValues();
        endingAction?.Invoke();
    }
}
