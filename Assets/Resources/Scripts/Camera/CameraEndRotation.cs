﻿using UnityEngine;

public class CameraEndRotation : MonoBehaviour
{
    [SerializeField] private float _speed = 3.5f;

    private Player _player;
    private Vector3 _target;
    private EnemyCounter _counter;
    private bool _canRotate = false;

    private void OnEnable()
    {
        _player = FindObjectOfType<Player>();
        _player.Died += OnEnded;
        _counter = FindObjectOfType<EnemyCounter>();
        _counter.AllEnemyDied += OnEnded;
    }

    private void OnDisable()
    {
        _player.Died -= OnEnded;
        _counter.AllEnemyDied -= OnEnded;
    }

    private void Update()
    {
        if (_canRotate)
        {
            transform.RotateAround(_target, Vector3.up, _speed * Time.unscaledDeltaTime);
        }
    }

    private void OnEnded()
    {
        _target = _player.transform.position;
        _canRotate = true;
    }
}
