﻿using System.Collections;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    [SerializeField] private bool _canShake;

    public void Shake(float duration, float magmitude)
    {
        if (_canShake)
        {
            StartCoroutine(Shaking(duration, magmitude));
        }
    }

    private IEnumerator Shaking(float duration, float magmitude)
    {
        Vector3 originalPosition = transform.localPosition;

        float elapsed = 0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magmitude;
            float y = Random.Range(-1f, 1f) * magmitude;

            transform.localPosition = new Vector3(x, y, originalPosition.z);

            elapsed += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = originalPosition;
    }
}
