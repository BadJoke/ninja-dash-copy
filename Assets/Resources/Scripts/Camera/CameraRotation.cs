﻿using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    [SerializeField] private Transform _player;
    [SerializeField] private Transform _lookPoint;
    [SerializeField] private float _speed = 3.5f;
    [SerializeField] private float _deadZone = 3.5f;

    private float _startPosition;
    private float _direction;
    private bool _canRoate = false;

    private void Start()
    {
        transform.LookAt(_lookPoint);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startPosition = Input.mousePosition.x;
        }

        if (_canRoate)
        {
            if (Input.GetMouseButton(0))
            {
                _direction = Input.mousePosition.x - _startPosition;
                if (Mathf.Abs(_direction) <= _deadZone)
                {
                    _direction = 0f;
                }
                float angle = _direction * _speed * Time.unscaledDeltaTime;

                transform.RotateAround(_player.position, Vector3.up, angle);
            }
        }
    }

    public void Activate()
    {
        _canRoate = true;
    }
}
