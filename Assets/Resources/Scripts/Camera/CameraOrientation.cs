﻿using UnityEngine;

public class CameraOrientation : MonoBehaviour
{
    void Update()
    {
        if (Camera.main.aspect < 1)
        {
            Camera.main.fieldOfView = 60;
        }  
        else
        {
            Camera.main.fieldOfView = 50;
        }
    }
}
