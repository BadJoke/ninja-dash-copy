﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image), typeof(Outline))]
public class EnemyUI : MonoBehaviour
{
    [SerializeField] private Color _deadColor;

    public void OnDied(Enemy enemy)
    {
        GetComponent<Image>().DOColor(_deadColor, 0.45f).SetUpdate(true);
        GetComponent<Outline>().DOColor(_deadColor, 0.45f).SetUpdate(true);
    }
}
