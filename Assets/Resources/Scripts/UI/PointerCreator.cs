﻿using System.Collections.Generic;
using UnityEngine;

public class PointerCreator : MonoBehaviour
{
    [SerializeField] private EnemyPointer _pointerPrefab;
    [SerializeField] private EnemySpawner _spawner;
    [SerializeField] private RectTransform _canvas;

    private List<Enemy> _enemies;

    private void OnEnable()
    {
        _spawner.Spawned += OnSpawned;
    }

    private void OnSpawned(List<Enemy> enemies)
    {
        _enemies = enemies;
        _spawner.Spawned -= OnSpawned;
    }

    public void Create()
    {
        foreach (var enemy in _enemies)
        {
            var pointer = Instantiate(_pointerPrefab, transform);
            pointer.Init(enemy, _canvas);
        }
    }
}
