﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class EnemyDigit : MonoBehaviour
{
    [SerializeField] private Text _text;

    private Transform _camera;
    private Sequence _blinking;
    private bool _isHided;

    private void Start()
    {
        FindObjectOfType<Player>().Died += Hide;
        GetComponent<Canvas>().worldCamera = Camera.main;
        _camera = Camera.main.transform;
        _isHided = false;
    }

    private void LateUpdate()
    {
        transform.LookAt(_camera);
    }

    public void Set(string digit)
    {
        if (digit == "" || digit == "0" || digit == null)
        {
            _text.text = "";
        }
        else
        {
            _text.text = digit;
        }
    }

    public string Get()
    {
        return _text.text;
    }

    public void Show()
    {
        _isHided = false;
        gameObject.SetActive(true);

        var sequence = DOTween.Sequence();
        sequence.SetUpdate(true);
        sequence.Append(transform.DOScale(1.35f, 0.4f));
        sequence.Append(transform.DOScale(1f, 0.1f));
        sequence.onComplete = StartBlinking;
    }

    public void Hide()
    {
        if (_isHided)
        {
            return;
        }

        _isHided = true;
        _blinking.Kill();

        var sequence = DOTween.Sequence();
        sequence.SetUpdate(true);
        sequence.Append(transform.DOScale(1.35f, 0.1f));
        sequence.Append(transform.DOScale(0f, 0.4f));
    }

    private void StartBlinking()
    {
        _blinking = DOTween.Sequence();
        _blinking.SetUpdate(true).SetEase(Ease.Linear);
        _blinking.Append(transform.DOScale(1.25f, 0.5f));
        _blinking.Append(transform.DOScale(1f, 0.5f));
        _blinking.SetLoops(-1);
    }
}
