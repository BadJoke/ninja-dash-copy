﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuDisabler : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private CameraRotation _cameraRotation;
    [SerializeField] private SkyboxRotation _skyboxRotation;
    [SerializeField] private GameObject _nextTutorials;
    [SerializeField] private GameObject _firstTutorial;
    [SerializeField] private RectTransform _restart;
    [SerializeField] private RectTransform _counter;
    [SerializeField] private GameNameHider _hider;
    [SerializeField] private CameraHandler _cameraHandler;
    [SerializeField] private PointerCreator _pointerCreator;

    private float _cameraSpeed = 1.4f;
    private Action _action;

    public UnityAction<Action> Restart;

    public void OnClick()
    {
        _action += StartGame;
        _action += _pointerCreator.Create;

        _firstTutorial.SetActive(false);
        _restart.DOLocalMoveX(_restart.localPosition.x - 80f, 0.8f);
        _counter.DOLocalMoveX(_counter.localPosition.x + 90f, 0.8f);
        _hider.Hide();
        _player.StoplIdleSwitching();
        _cameraHandler.RaiseCamera(0f, _cameraSpeed, _cameraSpeed, _action);

        GetComponent<Button>().interactable = false;
    }

    public void OnRestartClick()
    {
        Restart?.Invoke(RestartGame);
    }

    public void OnEndClick()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Gameplay");
    }

    private void StartGame()
    {
        _player.enabled = true;
        _skyboxRotation.enabled = false;
        _nextTutorials.SetActive(true);
        _cameraRotation.Activate();

        gameObject.SetActive(false);
    }

    private void RestartGame()
    {
        SceneManager.LoadScene("Gameplay");
    }
}
