﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class EndPanel : MonoBehaviour
{
    [SerializeField] private float _duration;
    [SerializeField] private Text _topMessage;
    [SerializeField] private Text _bottomMessage;
    [SerializeField] private Image _image;
    [SerializeField] private RectTransform _levelNumber;
    [SerializeField] private RectTransform _restart;
    [SerializeField] private EnemyCounter _counter;
    [SerializeField] private GameObject[] _disablingObjects;

    private bool _isWin;
    private const string WinTopMessage = "LEVEL CLEARED!";
    private const string WinBottomMessage = "TAP TO CONTINUE";
    private const string LoseTopMessage = "LEVEL FAILED!";
    private const string LoseBottomMessage = "TAP TO RESTART";

    private void Awake()
    {
        _counter.AllEnemyDied += OnAllEnemyDied;

        FindObjectOfType<Player>().Died += OnDied;
        FindObjectOfType<MenuDisabler>().Restart += OnRestart;

        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        _counter.AllEnemyDied -= OnAllEnemyDied;

        Time.timeScale = 1f;
        DOTween.Clear();
    }

    private void OnAllEnemyDied()
    {
        _isWin = true;
        PlayerPrefs.SetInt("Level", Options.CurrentLevel + 1);
        Options.RestartCount = 0;
        Invoke(nameof(Show), 0.3f);
    }

    private void OnDied()
    {
        _isWin = false;
        Options.RestartCount++;
        Invoke(nameof(Show), 0.3f);
    }

    private void OnRestart(Action action)
    {
        gameObject.SetActive(true);
        transform.localScale = Vector3.one;

        foreach (var item in _disablingObjects)
        {
            item.SetActive(false);
        }

        _levelNumber.gameObject.SetActive(false);
        _topMessage.gameObject.SetActive(false);
        _bottomMessage.gameObject.SetActive(false);
        _image.DOFade(1f, 0.5f).SetUpdate(true).onComplete = action.Invoke;
    }

    private void Show()
    {
        if (_isWin)
        {
            Options.RestartCount = 0;
            _counter.transform.DOLocalMoveX(_counter.transform.localPosition.x - 90f, 0.4f).SetUpdate(true);
        }

        gameObject.SetActive(true);
        transform.localScale = Vector3.one;

        _image.DOFade(0.12f, 0.3f);
        _restart.DOLocalMoveX(_restart.localPosition.x + 80f, 0.4f).SetUpdate(true);

        var sequence = DOTween.Sequence().SetUpdate(true);
        sequence.Append(_levelNumber.DOScale(1.2f, 0.2f));
        sequence.Append(_levelNumber.DOScale(0, 0.3f));
        sequence.Append(_topMessage.transform.DOLocalMoveY(_topMessage.transform.localPosition.y - 86f, 0.5f));

        if (_isWin)
        {
            _topMessage.text = WinTopMessage;
            _bottomMessage.text = WinBottomMessage;
        }
        else
        {
            _topMessage.text = LoseTopMessage;
            _bottomMessage.text = LoseBottomMessage;
        }
    }
}
