﻿using UnityEngine;
using UnityEngine.Events;

public class SecondTutorialDisabler : MonoBehaviour
{
    public UnityAction Hided;

    public void Hide()
    {
        Hided?.Invoke();
        gameObject.SetActive(false);
    }
}
