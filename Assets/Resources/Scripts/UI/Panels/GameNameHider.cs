﻿using DG.Tweening;
using UnityEngine;

public class GameNameHider : MonoBehaviour
{
    public void Hide()
    {
        var sequence = DOTween.Sequence();
        sequence.Append(transform.DOScale(Vector3.one * 1.2f, 0.2f));
        sequence.Append(transform.DOScale(Vector3.zero, 0.6f));
    }
}
