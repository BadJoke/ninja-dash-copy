﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class EnemyPointer : MonoBehaviour
{
    [SerializeField] private float _border;
    [SerializeField] private float _screenWidth;
    [SerializeField] private float _screenHeight;
    
    private Transform _target;
    private RectTransform _canvas;
    private Vector3 _fromPosition = new Vector3(Screen.width / 2, Screen.height / 2, 0f);
    private Vector3 _toPosition;
    private Camera _camera;
    private bool _isHided = true;
    private bool _isOutScreen;
    private float _minX;
    private float _maxX;
    private float _minY;
    private float _maxY;

    private void Awake()
    {
        var image = GetComponent<Image>().GetPixelAdjustedRect();
        _minX = image.width / 2 + _border;
        _minY = image.height / 2 + _border;
        _camera = Camera.main;
    }

    private void Update()
    {
        if (_target == null)
        {
            return;
        }

        _maxX = _canvas.rect.width - _minX;
        _maxY = _canvas.rect.height - _minY;
        _toPosition = _camera.WorldToScreenPoint(_target.position);
        _toPosition.z = 0f;

        if (Vector3.Dot(_target.position - _camera.transform.position, _camera.transform.forward) < 0)
        {
            _toPosition = -_toPosition;
        }

        _isOutScreen =
            _toPosition.x <= _minX * _canvas.localScale.x ||
            _toPosition.x >= _maxX * _canvas.localScale.x ||
            _toPosition.y <= _minY * _canvas.localScale.y ||
            _toPosition.y >= _maxY * _canvas.localScale.y;

        if (_isOutScreen)
        {
            if (_isHided)
            {
                Show();
            }

            float angleRad = Mathf.Atan2(_fromPosition.y - _toPosition.y, _fromPosition.x - _toPosition.x);
            float angle = angleRad * (180 / Mathf.PI) + 90f;

            transform.localEulerAngles = new Vector3(0f, 0f, angle);

            Vector3 arrowScreenPoint = _toPosition;
            arrowScreenPoint.x = Mathf.Clamp(arrowScreenPoint.x, _minX * _canvas.localScale.x, _maxX * _canvas.localScale.x);
            arrowScreenPoint.y = Mathf.Clamp(arrowScreenPoint.y, _minY * _canvas.localScale.y, _maxY * _canvas.localScale.y);
            transform.position = arrowScreenPoint;
        }
        else
        {
            if (_isHided == false)
            {
                Hide();
            }
        }
    }

    public void Init(Enemy target, RectTransform canvas)
    {
        _target = target.transform;
        _canvas = canvas;
        target.Died += Delete;
    }

    private void Delete(Enemy target)
    {
        target.Died -= Delete;
        Destroy(this);
    }

    private void Show()
    {
        transform.DOScale(1f, 0.1f).SetUpdate(true).onComplete = () =>
        {
            _isHided = false;
        };
    }

    private void Hide()
    {
        transform.DOScale(0f, 0.1f).SetUpdate(true).onComplete = () =>
        {
            _isHided = true;
        };
    }
}